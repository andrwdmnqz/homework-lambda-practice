import java.util.ArrayList;
import java.util.List;

public class FirstLetter {
    public static void main(String[] args) {
        List<String> surnameList = new ArrayList<>();
        surnameList.add("Jeffcoat");
        surnameList.add("Failes");
        surnameList.add("Quinney");
        surnameList.add("D'Hoe");
        surnameList.add("Candler");
        surnameList.add("Jelf");
        surnameList.add("Joynson");

        long result = surnameList
                .stream()
                .filter(s -> s.startsWith("J"))
                .count();
        System.out.println(result);
    }
}
