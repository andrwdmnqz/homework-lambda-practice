import java.util.Random;

public class EvenOddSum {
    public static void main(String[] args) {

        MyExpression evenExpression = (n) -> n % 2 == 0; // Визначає парність і повертає true якщо елемент ПАРНИЙ
        MyExpression oddExpression = (n) -> n % 2 == 1; // Визначає парність і повертає true якщо елемент НЕ ПАРНИЙ

        int[] exampleArray = new int[15];

        Random rnd = new Random();

        for(int i = 0; i < exampleArray.length; i++) {
            exampleArray[i] = rnd.nextInt(0, 20);
            if (i != exampleArray.length - 1) {
                System.out.print(exampleArray[i] + ", ");
            } else System.out.print(exampleArray[i] + "\n");
        }

        evenOddSum(exampleArray, evenExpression);
        evenOddSum(exampleArray, oddExpression);
    }

    public static void evenOddSum(int[] numberArray, MyExpression expression) {
        int resultAccumulator = 0;

        for(int number: numberArray) {
            if (expression.isEven(number)) {
                resultAccumulator += number;
            }
        }
        if(expression.isEven(1)) {
            System.out.println("Sum of odd: " + resultAccumulator);
        } else System.out.println("Sum of even: " + resultAccumulator);


    }

    @FunctionalInterface
    interface MyExpression {
        public boolean isEven(int n);
    }
}
