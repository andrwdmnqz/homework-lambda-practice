import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class EvenCount {
    public static void main(String[] args) {
        Set<Integer> randomSet = new HashSet<>();

        Random random = new Random();

        for(int i = 0; i < 15; i++) {
            randomSet.add(random.nextInt(0, 50));
        }
        System.out.println(randomSet);
        long result = randomSet
                .stream()
                .filter(el -> el % 2 == 0)
                .count();
        System.out.println(result);
    }
}
