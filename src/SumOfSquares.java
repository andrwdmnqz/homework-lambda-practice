import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SumOfSquares {
    public static void main(String[] args) {
        List<Integer> randomList = new ArrayList<>();
        Random random = new Random();

        for(int i = 0; i < 10; i++) {
            randomList.add(random.nextInt(0, 10));
        }

        int result = randomList
                .stream()
                .map(a -> a * a)
                .reduce(0, (ac, el) -> ac + el);

        randomList
                .stream()
                .map(a -> a * a)
                .forEach(el -> System.out.print(el + ", "));

        System.out.println("\n" + result);
    }
}
